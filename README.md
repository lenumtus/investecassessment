This Application is an Investec Assessment .

The Techstack
==================

- SpringBoot 
- H2 
- Flyway


The application DB
==================

- I use H2 as Storage

The migration tool used is Flyway , it will create the Table client.

Application
================

- The application will run on port 8090

Run the application
===================
The application uses maven as dependency management tool

- mvn spring-boot:run
 
Swagger
==================
-  The Swagger Url is : http://localhost:8090/swagger-ui/index.html
