package co.za.investecassignment.exceptions;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@ControllerAdvice
public class ResponseEntityCustomError extends ResponseEntityExceptionHandler {


    // error handle for @Valid
    @ExceptionHandler(ConstraintViolationException.class)
    protected ResponseEntity<Object>
    handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
                                 HttpHeaders headers,
                                 HttpStatus status, WebRequest request) {

        List<String> errors = new ArrayList<>();
                ex.getBindingResult().getAllErrors().forEach(fieldError -> {

                    errors.add(fieldError.getDefaultMessage());
                });
        return new ResponseEntity<>(new CustomExceptionResponse(errors, false), headers, status);

    }



    @ExceptionHandler(ItemNotFoundException.class)
    public final ResponseEntity<CustomExceptionResponse> handleItemNotFoundException(Exception ex, WebRequest request) {

        List<String> errors = new ArrayList<String>();
        errors.add(ex.getMessage());
        CustomExceptionResponse errorDetails = new CustomExceptionResponse(errors, false);
        return new ResponseEntity<>(errorDetails, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(Exception.class)
    public final ResponseEntity<CustomExceptionResponse> handleAllExceptions(Exception ex, WebRequest request) {

        List<String> errors = new ArrayList<String>();
        errors.add(ex.getMessage());
        CustomExceptionResponse errorDetails = new CustomExceptionResponse(errors, false);
        return new ResponseEntity<>(errorDetails, HttpStatus.BAD_REQUEST);
    }

}
