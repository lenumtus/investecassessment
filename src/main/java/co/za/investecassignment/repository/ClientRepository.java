package co.za.investecassignment.repository;

import co.za.investecassignment.domain.Client;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface ClientRepository extends JpaRepository<Client , UUID> {


    Optional<Client> findByIdNumber(String idNumber);

    Optional<Client> findByCellphone(String cellphoneNumber);

    List<Client> findByFirstName(String firstname);
}
