package co.za.investecassignment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InvestecAssignmentApplication {

    public static void main(String[] args) {
        SpringApplication.run(InvestecAssignmentApplication.class, args);
    }

}
