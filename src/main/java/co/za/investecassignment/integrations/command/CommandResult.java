package co.za.investecassignment.integrations.command;

import java.util.ArrayList;


public class CommandResult {
    ArrayList<String> errors;
    String recordId;
    Boolean success;


    public CommandResult(ArrayList<String> errors, String recordId, Boolean success) {
        super();
        this.errors = errors;
        this.recordId = recordId;
        this.success = success;
    }


    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public ArrayList<String> getErrors() {
        return errors;
    }

    public void setErrors(ArrayList<String> errors) {
        this.errors = errors;
    }

    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }


    public Boolean getSuccess() {
        return success;
    }

}
