package co.za.investecassignment.controller;

import co.za.investecassignment.api.requests.ClientDto;
import co.za.investecassignment.services.IClientService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Map;

@RestController
@RequestMapping("api/client")
@Slf4j
public class ClientController extends BaseController{

    @Autowired
    private IClientService clientService;

    @PostMapping("")
    public ResponseEntity<?> createClient(@RequestBody @Valid  ClientDto clientDto) {

        return this.generateReturnResult(clientService.addOrEditClient(clientDto));

    }



    @GetMapping("all")
    public ResponseEntity<?> listAllClient() {

        return this.generateReturnResult(clientService.listAllClient());

    }

    @GetMapping("search")
    public ResponseEntity<?> seachClient(@RequestParam Map<String,String> searchParams) {
        return this.generateReturnResult(clientService.searchClient(searchParams));

    }





}
