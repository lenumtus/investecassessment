package co.za.investecassignment.controller;

import co.za.investecassignment.integrations.command.CustomResult;
import org.springframework.http.ResponseEntity;


public class BaseController {


    protected ResponseEntity<?> generateReturnResult(CustomResult<?> commandResult) {


        return ResponseEntity.status(commandResult.getStatus()).body(commandResult.getResponse());
    }


}
