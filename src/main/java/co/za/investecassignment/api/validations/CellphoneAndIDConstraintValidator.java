package co.za.investecassignment.api.validations;

import co.za.investecassignment.api.requests.ClientDto;
import co.za.investecassignment.repository.ClientRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Component
@Slf4j
public class CellphoneAndIDConstraintValidator implements
        ConstraintValidator<CellphoneAndIDConstraint, ClientDto> {
    @Autowired
    ClientRepository clientRepository;


    @Override
    public void initialize(CellphoneAndIDConstraint constraintAnnotation) {

    }

    @Override
    public boolean isValid(ClientDto client, ConstraintValidatorContext context) {

        context.disableDefaultConstraintViolation();
        if(client.getCellphone()!= null && (client.getId()==null || client.getId()=="") &&  clientRepository.findByCellphone(client.getCellphone()).isPresent()){
            this.FailedContext(context , "Cellphone number "+client.getCellphone()+" already exist");
            return false;

        }
        if(!isIdNumberValid(client.getIdNumber())){
            this.FailedContext(context , "SA Id Number not Valid");
            return false;
        }

         if(((client.getId()==null || client.getId()=="") && clientRepository.findByIdNumber(client.getIdNumber()).isPresent())){
            this.FailedContext(context , "SA Id Number already exist");
            return false;
        }

        return true;
    }


    private void FailedContext(ConstraintValidatorContext context , String message){
        context.buildConstraintViolationWithTemplate(message).addConstraintViolation();
    }
    private static Boolean isIdNumberValid(String idNumber){
        char[] idchars = idNumber.toCharArray();
        int sum = 0;

        for (int i = 1; i <= idchars.length; i++) {
            int digit = Character.getNumericValue(idchars[idchars.length - i]);
            if ((i % 2) != 0) {
                sum += digit;
            } else {
                sum += digit < 5 ? digit * 2 : digit * 2 - 9;
            }
        }
        return (sum % 10) == 0;
    }

}
