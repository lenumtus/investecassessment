package co.za.investecassignment.api.requests;

import co.za.investecassignment.api.validations.CellphoneAndIDConstraint;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
@CellphoneAndIDConstraint
public class ClientDto {

    private String id;
    @NotNull(message = "The FirstName cannot be null")
    private String firstName;
    @NotNull(message = "The LastName cannot be null")
    private String lastName;
    private String cellphone;
    @NotNull(message = "The IdNumber cannot be null")
    private String idNumber;
    private String physicalAddress;
}
