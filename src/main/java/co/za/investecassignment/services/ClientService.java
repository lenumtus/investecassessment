package co.za.investecassignment.services;

import co.za.investecassignment.api.requests.ClientDto;
import co.za.investecassignment.domain.Client;
import co.za.investecassignment.exceptions.ItemNotFoundException;
import co.za.investecassignment.integrations.command.CustomResult;
import co.za.investecassignment.repository.ClientRepository;
import co.za.investecassignment.services.mapper.ClientToClientDtoMapper;
import co.za.investecassignment.services.searchClient.SearchFactory;
import co.za.investecassignment.services.searchClient.SearchOperation;
import lombok.extern.slf4j.Slf4j;
import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@Slf4j
public class ClientService implements IClientService{


    @Autowired
    ClientRepository clientRepository;
    @Autowired
    private SearchFactory searchFactory;
    private ClientToClientDtoMapper mapper = Mappers.getMapper(ClientToClientDtoMapper.class);

    public CustomResult<?> addOrEditClient(ClientDto clientDto){
        if(clientDto.getId()!=null && clientDto.getId()==""){
            clientDto.setId(UUID.randomUUID().toString());
        }
        Client client = mapper.clientDtoToClient(clientDto);
        Client newAddedClient = clientRepository.save(client);
        return new CustomResult<>(newAddedClient , HttpStatus.OK , true) ;
    }

    public CustomResult<?> listAllClient(){


            List<ClientDto> clients = clientRepository.findAll().stream().map(mapper::clientToClientDto).collect(Collectors.toList());


        return new CustomResult<>(clients , HttpStatus.OK , true) ;
    }

    @Override
    public CustomResult<?> searchClient(Map<String,String> searchParams) {
        Map.Entry<String,String> entry = searchParams.entrySet().iterator().next();
        searchFactory.initialize();
        SearchOperation searchOperation = searchFactory.getOperation(entry.getKey())
               .orElseThrow(()->new ItemNotFoundException("the search key '"+ entry.getKey() +"' is not suppported ! Please use ('firstname' or 'idnumber' or 'cellphone' )"));

        return new CustomResult<>(searchOperation.SearchClient(entry.getValue()), HttpStatus.OK , true);
    }


}
