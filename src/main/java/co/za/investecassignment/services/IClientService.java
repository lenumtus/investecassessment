package co.za.investecassignment.services;

import co.za.investecassignment.api.requests.ClientDto;
import co.za.investecassignment.integrations.command.CustomResult;

import java.util.Map;

public interface IClientService {

    public CustomResult<?> addOrEditClient(ClientDto clientDto);
    public CustomResult<?> listAllClient ();
    public CustomResult<?> searchClient(Map<String,String> searchParams);
}
