package co.za.investecassignment.services.mapper;

import co.za.investecassignment.api.requests.ClientDto;
import co.za.investecassignment.domain.Client;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring" , uses = {})
public interface ClientToClientDtoMapper {

    ClientDto clientToClientDto(Client client);
    @Mapping(target="java(UUID.fromString(client.id))")
    Client clientDtoToClient(ClientDto clientDto);
}
