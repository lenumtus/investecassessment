package co.za.investecassignment.services.searchClient;

import co.za.investecassignment.api.requests.ClientDto;

import java.util.List;

public interface SearchOperation {

    List<ClientDto> SearchClient(String searchValue);
}
