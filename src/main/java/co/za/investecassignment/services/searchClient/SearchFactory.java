package co.za.investecassignment.services.searchClient;


import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Slf4j
@Component
public class SearchFactory {


    @Autowired
    private  SearchByFirstNameOperation searchByFirstNameOperation;
    @Autowired
    private  SearchByIdNumberOperation searchByIdNumberOperation;
    @Autowired
    private  SearchByCellphoneOperation searchByCellphoneOperation;
    Map<String, SearchOperation> searchOperationMap = new HashMap<>();
    public void initialize(){
        searchOperationMap.put("firstname" , searchByFirstNameOperation);
        searchOperationMap.put("idnumber" , searchByIdNumberOperation);
        searchOperationMap.put("cellphone" , searchByCellphoneOperation);
    }
    public   Optional<SearchOperation> getOperation(String operator) {
        return Optional.ofNullable(searchOperationMap.get(operator));
    }
}
