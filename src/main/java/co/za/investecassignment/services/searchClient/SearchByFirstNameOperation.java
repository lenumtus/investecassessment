package co.za.investecassignment.services.searchClient;

import co.za.investecassignment.api.requests.ClientDto;
import co.za.investecassignment.repository.ClientRepository;
import co.za.investecassignment.services.mapper.ClientToClientDtoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class SearchByFirstNameOperation implements SearchOperation{
    @Autowired
    private ClientRepository repository;
    @Autowired
    ClientToClientDtoMapper mapper;

    @Override
    public List<ClientDto> SearchClient(String searchValue) {
          return repository.findByFirstName(searchValue)
                  .stream()
                  .map(mapper::clientToClientDto).collect(Collectors.toList());


    }
}
