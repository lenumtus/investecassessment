package co.za.investecassignment.services.searchClient;

import co.za.investecassignment.api.requests.ClientDto;
import co.za.investecassignment.repository.ClientRepository;
import co.za.investecassignment.services.mapper.ClientToClientDtoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class SearchByIdNumberOperation implements SearchOperation{

    @Autowired
    ClientRepository repository;
    @Autowired
    ClientToClientDtoMapper mapper;
    @Override
    public List<ClientDto> SearchClient(String searchValue) {
        List<ClientDto> clientDtos = new ArrayList<>();
        repository.findByIdNumber(searchValue)
                  .ifPresent(client -> clientDtos.add(mapper.clientToClientDto(client)));

        return clientDtos;
    }
}
