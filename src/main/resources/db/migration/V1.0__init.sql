/*Table structure for table Client  */
CREATE TABLE IF NOT EXISTS client (
    id uuid not null,
    first_name varchar(50) not null,
    last_name varchar(50) not null,
    id_number varchar(50) unique not null,
    cellphone varchar(50) unique ,
    physical_address varchar(255) not null,
    update_date timestamp not null default CURRENT_TIMESTAMP,
    create_date timestamp not null default CURRENT_TIMESTAMP,
    PRIMARY KEY(id)
) ;
